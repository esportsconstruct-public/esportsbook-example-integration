# Esportsbook integration

This project shows Esportsbook integration options

## Cases

### Case 1

Footer and header provided by the client. 
This is a separate js file (web-component) for the footer and a separate one for the header. 
The login logic can contain either one of these files, or a redirect/iframe to a separate url.

![plot](./assets/1.png)

This is the best possible option.\
This allows us:
 * To use the correct definition of the user's device and its orientation.
 * Installation of PWA application.
 * Use native push notifications

#### Requirements
 * The client must provide js files for their header and footer. The header must contain the login logic.
   Or client must provide url for iframe for form login.\
   Or provide an API to receive a token by login and password, then the login form will be copied from the client page

### Case 2

The project and betslips panel are sablings. Project in iframe, betslips panel is web-component

![plot](./assets/2.png)

#### Cons:
 * It is not possible to notify the user during DDoS attacks or if his region is not served (by means of hosting)

#### Installation

1. Import integration script file
    ```html
     <script
         type="text/javascript"
         src="URL/esportsbook-iframe.js"
     ></script>
    ```
   Change `URL` to correct value. Value provided from Esportsconstruct
   
2. Create a container for application. It should be a regular full-width div.
    ```html
    <div id="esportsbook"></div>
    ```
4. Init JS script\
   Example:
    ```html
        <script type="text/javascript">
            /**
             *  Implement the get current lang code here.
             *  Example: var currentLang = window.location.pathname.split("/")[1] + "-xx";
             *  We accept any second part code after a hyphen. "en-xx" - allowed
             * @type {string}
             */
            var currentLang = "en-gb";

            /**
             * This variable contains the HtmlElement with the maximum window width
             * Change to get the container element
             * @type {HTMLElement}
             */
            var esportsbookElement = document.getElementById("esportsbook");

            if (esportsbookElement) {
                var esportsbookBuilder = new EsportsbookBuilder(esportsbookElement, {
                    codeName: "YOUR_CODE_NAME_IN_ESPORTSCONSTRUCT", // provided from Esportsconstruct
                    appUrl: "IFRAME_URL", // provided from Esportsconstruct
                    router: {
                        skipSectionsCountTo: 0, // Example: if you use https://your-url/en is skipSectionsCountTo: 1
                        langCode: currentLang,
                        /**
                         * Here you can specify any parameters that you want to pass to the frame. These parameters will be hidden for the user.
                         * Example:
                         * hideParams: { 
                         *    brandid: "MYBRAND",
                         *    frontendid: "MYFRONTENT",
                         *    productid: "MYPRODUCT",
                         *    parent: "PARENT_URL"
                         *    deviceType: "desktop"
                         * } 
                         */
                         hideParams: {
                            /**
                             * If you have a detector device, please use it.
                             * Values: "desktop" | "tablet" | "mobile"
                             * Default: "desktop" 
                             */
                            deviceType: "desktop"
                         }
                    },
                    /**
                     * We will give panel field values for your deployment
                     */
                    panel: { // provided from Esportsconstruct
                        jsUrl: "BETSLIPS_PANEL_JS_URL",
                        assetsUrl: "BETSLIPS_PANEL_ASSETS_URL",
                        assetsUrlCommon: "ESPORTSBOOK_COMMON_ASSETS_URL",
                        theme: "THEME_CODE",
                        /**
                        * injectTo optional parameter. If you do not specify it, the script will be added at the end of the body element
                        */
                        // injectTo: esportsbookElement
                    },
                    callbacks: {
                        /**
                         * This callback will be triggered when the user clicks the login button in our application.
                         * This should redirect to your login page or open a modal or whatever.
                         */
                        clickToLogin: () => {},
                        /**
                         * Callback of the loaded panel, it's time to restore the session if it was
                         */
                        panelLoaded: () => {
                            setTimeout(() => {
                                 const token = "test";
                                 esportsbookBuilder.esportsbook.sendLogin(
                                     new Date().toISOString(),
                                     new Date(new Date().getTime() + 60000).toISOString(),
                                     token
                                 );
                            }, 100);
                        },
                        /**
                         * A few seconds before the expirationTime passes. We call this callback to get a new token and new expiration dates
                         * @returns {Promise}
                         */
                        sendExtendUserSession: () => {
                            return new Promise((resolve, reject) => {
                                const token = "test";

                                resolve({
                                    creationTime: new Date().toISOString(),
                                    expirationTime: new Date(
                                        new Date().getTime() + 60000
                                    ).toISOString(),
                                    sessionToken: token
                                });
                            });
                        },
                        /**
                         * Implement the language change logic (from esportsbook) here
                         * We have a select with a choice of language inside app, maybe you just replace it in your url, maybe you will do something more complex
                         * @param langCode
                         */
                        changeLangCode: (langCode) => {
                            currentLang = langCode;
                        }
                    }
                });

                /**
                 * You can call this when your page language changes without reloading
                 */
                esportsbookBuilder.esportsbook.changeLangCode(currentLang);

                /**
                 * Used to correctly position absolute elements within a frame. Must be called at least once in bootstrap
                 * @param {number} headerHeight
                 * @param {number} footerHeight
                 * @param {number} headerFixedHeight
                 * @param {number} footerFixedHeight
                 */
                esportsbookBuilder.esportsbook.changeHeaderFooterSizes(
                    document.getElementsByClassName("header")[0].clientHeight,
                    document.getElementsByClassName("footer")[0].clientHeight,
                    document.getElementsByClassName("header-fixed")[0].clientHeight,
                    0
                );
            }
        </script>
    ```
   Change `// provided from Esportsconstruct` to correct value

### Case 3

The project is inside a frame

![plot](./assets/3.png)

#### Cons:
 * The betslips panel jerks when scrolling. It is not possible to do this smoothly because the frame knows nothing about scrolling in the parent element.
 * Any modal window with absolute positioning cannot be displayed for the whole application. It is always displayed inside the iframe.
 * It is not possible to notify the user during DDoS attacks or if his region is not served

#### Installation

Installation does not differ from `case 2`. In addition, you do not need to use settings for the panel
